input = [1, 2, 4, 3, 10, 6, 9, 7, 12, 5, 13, 8, 14, 11]

output = ""
i = 1
for a in input:
    string = "x_{" + str(i) + "} =" + str(a) + ", "
    output = output + string
    i = i + 1
print('$\{'+output[:-2]+'\}$')