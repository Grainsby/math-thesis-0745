#Simple program to calculate coeficient of leading monomial for C3 U C4
import numpy as np
import sympy as sp
from sympy import symbols, collect, expand, poly, degree_list, prod, Poly
import time


cyclicSize = [4]
progress = []
prev = -1
for n in cyclicSize:
    prev += n
    progress.append(prev)

numVertices = progress[-1] +1
threshold = progress[-1]

print(progress)
print(cyclicSize)

coefficients = symbols('a0:%d'%numVertices)

print(coefficients)

accumulator = 1
for i in range(numVertices):
    for j in range(i):
        term1 = coefficients[j]
        term2 = coefficients[i]
        currentP = term2-term1
        currentP = currentP
        accumulator = accumulator * currentP
        # print(coefficients)
        # print(currentP)
print("left side poly")
print(accumulator)
print(accumulator.as_poly())
# print(degree_list(leftSide))
# print(leftSide.monoms())
# print([prod(x**k for x, k in zip(leftSide.gens, mon)) for mon in leftSide.monoms()])

currentCycle = 0
change = 0
edges = []
for i in range(numVertices):
    if progress[currentCycle] < i:
        currentCycle += 1
        change = change + cyclicSize[currentCycle-1]
    for j in range(i+1, progress[currentCycle] + 1):
        if not (i, i + 1) in edges:
            edges.append((i, i+1))
        # term1 = coefficients[i] - coefficients[(i+1)] #edge ji
        if ((j+1) % (progress[currentCycle] + 1)) == 0:
            jnext = change
        else:
            jnext = (j+1) % (progress[currentCycle] + 1)
        if not (j, jnext) in edges:
            edges.append((j, jnext))
        # print((i, i+1), (j, jnext))
        # term2 = coefficients[j] - coefficients[jnext] #edge lk
        # currentP = term1-term2
        # accumulator = accumulator * currentP
        # print(coefficients)
        # print(currentP)

for a in edges:
    print(a)

variablesToReduce = []
for i in range(len(edges)):
    print(variablesToReduce)
    for a in variablesToReduce:
        start_time = time.time()
        accumulator = accumulator + sum(a ** n for n in range(numVertices))
        coeffs = Poly(accumulator, a).coeffs()
        accumulator = sum(a ** n * coeffs[-(n + 1)] for n in range(numVertices))  # the +1 is to get 0th order
        accumulator = accumulator - sum(a ** n for n in range(numVertices))
        print(a)
        print("--- %s seconds ---" % (time.time() - start_time))
    variablesToReduce = []
    for j in range(i):
        term1 = coefficients[edges[j][0]] - coefficients[edges[j][1]]
        term2 = coefficients[edges[i][0]] - coefficients[edges[i][1]]
        currentP = term1 - term2
        print(currentP)
        accumulator = accumulator * currentP
        if not coefficients[edges[j][0]] in variablesToReduce:
            variablesToReduce.append(coefficients[edges[j][0]])
        if not coefficients[edges[j][1]] in variablesToReduce:
            variablesToReduce.append(coefficients[edges[j][1]])
        if not coefficients[edges[i][0]] in variablesToReduce:
            variablesToReduce.append(coefficients[edges[i][0]])
        if not coefficients[edges[i][1]] in variablesToReduce:
            variablesToReduce.append(coefficients[edges[i][1]])
print("main polynomial")
print(accumulator.as_poly())


#############################################
"""
currentCycle = 0
change = 0
for i in range(numVertices):
    if progress[currentCycle] < i:
        for a in coefficients[change:progress[currentCycle] + 1]:
            start_time = time.time()
            accumulator = accumulator + sum(a ** n for n in range(numVertices))
            coeffs = Poly(accumulator, a).coeffs()
            accumulator = expand(sum(a ** n * coeffs[-(n + 1)] for n in range(numVertices)))  # the +1 is to get 0th order
            accumulator = accumulator - sum(a ** n for n in range(numVertices))
            print(a)
            print("--- %s seconds ---" % (time.time() - start_time))
        currentCycle += 1
        change = change + cyclicSize[currentCycle-1]
    for j in range(i+1, progress[currentCycle] + 1):
        term1 = coefficients[i] - coefficients[(i+1)] #edge ji
        if ((j+1) % (progress[currentCycle] + 1)) == 0:
            jnext = change
        else:
            jnext = (j+1) % (progress[currentCycle] + 1)
        term2 = coefficients[j] - coefficients[jnext] #edge lk
        currentP = term1-term2
        accumulator = accumulator * currentP
        # print(coefficients)
        # print(currentP)
print(accumulator)
# print(rightSide.as_poly())

# print(accumulator.as_poly())
# print(output.as_poly())
# order = 3
"""
"""
for a in coefficients:
    output = output + sum(a**n for n in range(numVertices))
    coeffs = Poly(output, a).coeffs()
    # print(coeffs)
    output = expand(sum(a**n * coeffs[-(n+1)] for n in range(numVertices))) # the +1 is to get 0th order
    output = output - sum(a**n for n in range(numVertices))
    print(output.as_poly())
"""
print("--- %s seconds ---" % (time.time() - start_time))

for a in coefficients:
    start_time = time.time()
    accumulator = accumulator + sum(a ** n for n in range(numVertices))
    coeffs = Poly(accumulator, a).coeffs()
    accumulator = sum(a ** n * coeffs[-(n + 1)] for n in range(numVertices))  # the +1 is to get 0th order
    accumulator = accumulator - sum(a ** n for n in range(numVertices))
    print(a, accumulator)
    print("--- %s seconds ---" % (time.time() - start_time))
print(accumulator)
