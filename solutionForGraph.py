# Simple program to calculate coeficient of leading monomial for C3 U C4
import numpy as np
import sympy as sp
from sympy import symbols, collect, expand, poly, degree_list, prod, Poly
import time
from itertools import permutations, combinations
cyclicGraphs = [[5, 5], [7, 7, 3]]
print(cyclicGraphs)
for a in cyclicGraphs:
    print(list(a))
    cyclicSize = list(a)
    progress = []
    prev = -1

    for n in cyclicSize:
        prev += n
        progress.append(prev)

    numVertices = progress[-1] + 1
    threshold = progress[-1]


    coefficients = symbols('a0:%d' % numVertices)

    # print(coefficients)
    currentCycle = 0
    change = 0
    edges = []
    for i in range(numVertices):
        if progress[currentCycle] < i:
            currentCycle += 1
            change = change + cyclicSize[currentCycle - 1]
        for j in range(i + 1, progress[currentCycle] + 1):
            if not (i, i + 1) in edges:
                edges.append((i, i + 1))
            # term1 = coefficients[i] - coefficients[(i+1)] #edge ji
            if ((j + 1) % (progress[currentCycle] + 1)) == 0:
                jnext = change
            else:
                jnext = (j + 1) % (progress[currentCycle] + 1)
            if not (j, jnext) in edges:
                edges.append((j, jnext))

    variablesToReduce = []
    accumulator = 1
    for i in range(len(edges)):
        for j in range(i):
            term1 = coefficients[edges[j][0]] - coefficients[edges[j][1]]
            term2 = coefficients[edges[i][0]] - coefficients[edges[i][1]]
            currentP = term1 - term2
            accumulator = accumulator * currentP

    print("solving")
    count = 0
    #numbers = range(5, len(coefficients) + 1)
    numbers = [5, 1, 8, 4, 12, 7, 15]
    p = permutations(numbers)

    start_time = time.time()
    for j in p:

        it = [17, 2, 14, 3, 10, 6, 9, 13, 16, 11] + list(j)
        a = dict(zip(coefficients, it))
        num = accumulator.subs(a)
        if num != 0:
            print(it)
            break
    print(cyclicSize)
    print("--- %s seconds ---" % (time.time() - start_time))


