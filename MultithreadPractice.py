from joblib import Parallel, delayed
import multiprocessing
import time
from itertools import permutations

# what are your inputs, and what operation do you want to
# perform on each input. For example...
from sympy import symbols

num_cores = multiprocessing.cpu_count()

def parallel_func(j):
    count = 0
    unique = {}
    the_coefficient = 0
    solution = None
    a = dict(zip(coefficients, j))
    num = accumulator.subs(a)
    if num != 0:
        solution = a
        if num not in unique.keys():
            frac = num / N
            unique[num] = [1, frac]
        else:
            unique[num][0] += 1
            frac = unique[num][1]
        the_coefficient += frac
        count = count + 1
    return count, unique, the_coefficient, solution


def recur(i, n, out, index):
    global cyclicGraphs
    # if sum becomes n, print the combination
    if n == 0:
        cyclicGraphs.append(out[:index])

    # start from previous element in the combination till n
    for j in range(i, n + 1):
        # place current element at current index
        out[index] = j

        # recur with reduced sum
        recur(j, n - j, out, index + 1)


def latexIt(unique, N, total):
    acc = ""
    for key, value in unique.items():
        cur = "\\frac{"+ str(key) + "}{" + str(N) + "} * " + str(value[0]) + " + "
        acc = acc + cur
    acc = acc[:-2] + "= " + str(total)
    return acc

for n in range(2, 8):
    out = [None] * n

    cyclicGraphs = []
    # print all combination of numbers from 2 to n having sum n
    recur(2, n, out, 0)

    for a in cyclicGraphs:
        print(list(a))
        cyclicSize = list(a)
        progress = []
        prev = -1
        for n in cyclicSize:
            prev += n
            progress.append(prev)

        numVertices = progress[-1] + 1
        threshold = progress[-1]

        coefficients = symbols('a0:%d' % numVertices)

        # print(coefficients)

        accumulator = 1
        for i in range(numVertices):
            for j in range(i):
                term1 = coefficients[j]
                term2 = coefficients[i]
                currentP = term1 - term2
                accumulator = accumulator * currentP
        leftSide = accumulator
        currentCycle = 0
        change = 0
        edges = []
        for i in range(numVertices):
            if progress[currentCycle] < i:
                currentCycle += 1
                change = change + cyclicSize[currentCycle - 1]
            for j in range(i + 1, progress[currentCycle] + 1):
                if not (i, i + 1) in edges:
                    edges.append((i, i + 1))
                # term1 = coefficients[i] - coefficients[(i+1)] #edge ji
                if ((j + 1) % (progress[currentCycle] + 1)) == 0:
                    jnext = change
                else:
                    jnext = (j + 1) % (progress[currentCycle] + 1)
                if not (j, jnext) in edges:
                    edges.append((j, jnext))

        variablesToReduce = []
        for i in range(len(edges)):
            for j in range(i):
                term1 = coefficients[edges[j][0]] - coefficients[edges[j][1]]
                term2 = coefficients[edges[i][0]] - coefficients[edges[i][1]]
                currentP = term1 - term2
                accumulator = accumulator * currentP


        def calculate_N(inputList):
            accumulator = 1
            for a in inputList:
                for b in inputList:
                    if a != b:
                        accumulator = accumulator * (a - b)
            return accumulator


        count = 0
        numbers = range(1, len(coefficients) + 1)
        N = calculate_N(numbers)
        p = permutations(numbers)
        unique = {}
        the_coefficient = 0
        solution = None
        start_time = time.time()

        ret = Parallel(n_jobs=num_cores)(delayed(parallel_func)(j) for j in p)
        print("--- %s seconds ---" % (time.time() - start_time))

        start_time = time.time()
        for r in ret:
            count = count + r[0]
            the_coefficient + the_coefficient + r[2]
            if r[3] is not None:
                solution = r[3]
            for key, value in r[1].items():
                if key not in unique.keys():
                    unique[key] = r[1][key]
                else:
                    unique[key][0] += 1
        print("--- %s seconds ---" % (time.time() - start_time))


        print(cyclicSize)
        print("a solution:", solution)
        print("total solutions:", count)
        print("unique solutions:", len(unique), unique)
        print("coefficients", the_coefficient)
        print("divisor", N)
        latexCode = latexIt(unique, N, the_coefficient)
        print(latexCode)

