# Simple program to calculate coeficient of leading monomial for C3 U C4
import numpy as np
import sympy as sp
from sympy import symbols, collect, expand, poly, degree_list, prod, Poly
import time
from itertools import permutations, combinations

def parallel_func(j):
    count = 0
    unique = {}
    the_coefficient = 0
    a = dict(zip(coefficients, j))
    num = accumulator.subs(a)
    if num != 0:
        if num not in unique.keys():
            frac = num / N
            unique[num] = [1, frac]
        else:
            unique[num][0] += 1
            frac = unique[num][1]
        # print(a)
        # print(num)
        the_coefficient += frac
        count = count + 1
    return count, unique, the_coefficient


def latexIt(unique, N, total):
    acc = ""
    for key, value in unique.items():
        cur = "\\frac{"+ str(key) + "}{" + str(N) + "} * " + str(value[0]) + " + "
        acc = acc + cur
    acc = acc[:-2] + "= " + str(total)
    return acc


def recur(i, n, out, index):
    global cyclicGraphs
    # if sum becomes n, print the combination
    if n == 0:
        cyclicGraphs.append(out[:index])

    # start from previous element in the combination till n
    for j in range(i, n + 1):
        # place current element at current index
        out[index] = j

        # recur with reduced sum
        recur(j, n - j, out, index + 1)

for n in range(4, 6):
    out = [None] * n

    cyclicGraphs = []
    # print all combination of numbers from 2 to n having sum n
    recur(2, n, out, 0)

    for a in cyclicGraphs:
        print(list(a))
        cyclicSize = list(a)
        progress = []
        prev = -1
        for n in cyclicSize:
            prev += n
            progress.append(prev)

        numVertices = progress[-1] + 1
        threshold = progress[-1]

        # print(progress)
        # print(cyclicSize)

        coefficients = symbols('a0:%d' % numVertices)

        # print(coefficients)

        accumulator = 1
        for i in range(numVertices):
            for j in range(i):
                term1 = coefficients[j]
                term2 = coefficients[i]
                currentP = term1 - term2
                # currentP = currentP.as_poly()
                accumulator = accumulator * currentP
                # print(coefficients)
                # print(currentP)
        # print("left side poly")
        leftSide = accumulator
        # print(accumulator)
        # print(leftSide)
        # print(degree_list(leftSide))
        # print(leftSide.monoms())
        # print([prod(x**k for x, k in zip(leftSide.gens, mon)) for mon in leftSide.monoms()])
        currentCycle = 0
        change = 0
        edges = []
        for i in range(numVertices):
            if progress[currentCycle] < i:
                currentCycle += 1
                change = change + cyclicSize[currentCycle - 1]
            for j in range(i + 1, progress[currentCycle] + 1):
                if not (i, i + 1) in edges:
                    edges.append((i, i + 1))
                # term1 = coefficients[i] - coefficients[(i+1)] #edge ji
                if ((j + 1) % (progress[currentCycle] + 1)) == 0:
                    jnext = change
                else:
                    jnext = (j + 1) % (progress[currentCycle] + 1)
                if not (j, jnext) in edges:
                    edges.append((j, jnext))
                # print((i, i+1), (j, jnext))
                # term2 = coefficients[j] - coefficients[jnext] #edge lk
                # currentP = term1-term2
                # accumulator = accumulator * currentP
                # print(coefficients)
                # print(currentP)

        for i in range(len(edges)):
            for j in range(i):
                term1 = coefficients[edges[j][0]] - coefficients[edges[j][1]]
                term2 = coefficients[edges[i][0]] - coefficients[edges[i][1]]
                currentP = term1 - term2
                accumulator = accumulator * currentP
        print(accumulator)

        # output = rightSide * leftSide
        # print(output)

        # print(accumulator.as_poly())
        # print(output.as_poly())
        # order = 3
        """
        for a in coefficients:
            output = output + sum(a**n for n in range(numVertices))
            coeffs = Poly(output, a).coeffs()
            # print(coeffs)
            output = expand(sum(a**n * coeffs[-(n+1)] for n in range(numVertices))) # the +1 is to get 0th order
            output = output - sum(a**n for n in range(numVertices))
            print(output.as_poly())
        """
        """
        for a in coefficients:
            start_time = time.time()
            output = output + sum(a ** n for n in range(numVertices))
            coeffs = Poly(output, a).coeffs()
            output = expand(sum(a ** n * coeffs[-(n + 1)] for n in range(numVertices)))  # the +1 is to get 0th order
            output = output - sum(a ** n for n in range(numVertices))
            print(a, output)
            print("--- %s seconds ---" % (time.time() - start_time))
        print(output)
        """


        def calculate_N(inputList):
            accumulator = 1
            for a in inputList:
                for b in inputList:
                    if a != b:
                        accumulator = accumulator * (a - b)
            return accumulator


        numbers = range(1, len(coefficients) + 1)
        N = calculate_N(numbers)
        p = permutations(numbers)

        count = 0
        unique = {}
        the_coefficient = 0
        start_time = time.time()
        for j in p:
            a = dict(zip(coefficients, j))
            num = accumulator.subs(a)
            if num != 0:
                if num not in unique.keys():
                    frac = num / N
                    unique[num] = [1, frac]
                else:
                    unique[num][0] += 1
                    frac = unique[num][1]
                print(num, a)
                # print(num)
                the_coefficient += frac
                count = count + 1
        print(cyclicSize)
        print("total solutions:", count)
        print("unique solutions:", len(unique), unique)
        print("coefficients", the_coefficient)
        print("divisor", N)
        latexCode = latexIt(unique, N, the_coefficient)
        print(latexCode)
        print("--- %s seconds ---" % (time.time() - start_time))

