# Math Thesis 0745
# Original code
The file main.py is the original method for calculating coefficients. It was able to go up to graphs of order 7.
# Coefficient Formula
The file noSiimplification.py is the linear version of applying the coefficient formula.
The file MultithreadPractice.py was the parallelized version of applying the coefficient formula
#Finding a Single Graceful Difference Label
The file solutionForGraph.py was used to find a single graceful difference labeling for given graph or list of graphs.
# Helper Functions
The file coefficientFinder.py and solutionsToLatex.py are helper scripts to convert findings into latex commands. Making things easier to typeset.